from datetime import date, timedelta
from zipfile import ZipFile
import pandas as pd
import os
import shutil
from jb_sharepoint_api import JB_sharepoint_API as JBapi


class TempDirectories:
    def __init__(self, lstTempDirs):
        self.lstTempDirs = lstTempDirs

    def __enter__(self):
        for dir in self.lstTempDirs:
            os.mkdir(dir)
            print("[Temporary directory created]: " + dir)

    def __exit__(self, exc_type, exc_value, traceback):
        for dir in self.lstTempDirs:
            shutil.rmtree(dir)
            print("[Temporary directory deleted]: " + dir)


class GetSentimentDataDK(TempDirectories):

    # Initiate variables
    def __init__(
        self,
        sSource="Borsen",
        sSPDirDown: str = "Shared Documents/SentimentDK/SentimentFiles",
        sdate: date = date(2020, 11, 18),
        edate: date = date.today() - timedelta(2),
        lstModelNames: list = [
            "SENTIDA",
            "BERT_Alexandra_Instituttet",
            "Bert_DaNLP_ExtendedVocab",
            "Bert_DaNLP_Head_ExtendedVocab",
            "Bert_DaNLP_All_ExtendedVocab",
            "Bert_DaNLP_Head_All_ExtendedVocab",
            "Combined_Bert_Sentida",
        ],
    ):
        

        self.oJBapi = JBapi.JB_sharepoint_API()
        self.sSource = sSource
        self.sSPDirDown = sSPDirDown
        self.sTempSentimentDir = os.path.abspath("tempSentimentDK")
        self.sZipFileDown = "{}_{}.zip"
        self.sFileNameDown = "{}_{}_{}.csv"
        self.sdate = sdate
        self.delta = edate - sdate
        self.lstModelNames = lstModelNames

    def DownloadFromSP(self):

        with TempDirectories([self.sTempSentimentDir]):

            # Download zip-file via JB_Sharepoint_API.
            for i in range(self.delta.days + 1):
                date = str(self.sdate + timedelta(days=i))
                sZipFileDown = self.sZipFileDown.format(self.sSource, date)
                self.Load_FromSP_ToTempFolder(
                    self.sSPDirDown,
                    self.sTempSentimentDir,
                    sZipFileDown,
                )
            # Unpack zipfiles
            lstZipFiles = os.listdir(self.sTempSentimentDir)
            for sZipFile in lstZipFiles:
                with ZipFile(
                    os.path.join(self.sTempSentimentDir, sZipFile), "r"
                ) as zip_ref:
                    zip_ref.extractall(self.sTempSentimentDir)

            # Import files and create dictionary of dataframes
            dicSentiment = {}
            lstSentimentFiles = os.listdir(self.sTempSentimentDir)
            for sModelName in self.lstModelNames:
                lstModelFiles = [
                    sFileName
                    for sFileName in lstSentimentFiles
                    if sModelName in sFileName
                ]
                if lstModelFiles:
                    dicSentiment[sModelName] = self.ImportCSVFromTempFolder(
                        self.sTempSentimentDir, lstModelFiles
                    )

            return dicSentiment

    # Download file from Share Point to temporary folder.
    def Load_FromSP_ToTempFolder(self, sDirSP, sTempDir, sFileNameSP):
        try:
            self.oJBapi.downloadFile(sDirSP, sTempDir, sFileNameSP)
        # Catch if file does not exist on Sharepoint and delete empty file
        # created in temp folder (i.e. sTempDir)
        except Exception as e:
            print("[SharePoint error message]: " + str(e))
            os.remove(os.path.join(sTempDir, sFileNameSP))

    # Read csv files from temporary folder
    def ImportCSVFromTempFolder(self, sFolderName, lstFileNames):
        return pd.concat(
            (self.ReadCSV(os.path.join(sFolderName, f)) for f in lstFileNames),
            axis=0,
            ignore_index=True,
        )

    def ReadCSV(self, sFilePath):
        return pd.read_csv(
            sFilePath,
            index_col=None,
            header=0,
            encoding="ISO-8859-1",
            delimiter=";",
            quotechar='"',
            decimal=",",
        )


testGetSentimentDataDK = GetSentimentDataDK(
    sdate=date(2020, 11, 18), edate=date(2020, 11, 19)
)

dictDFs = testGetSentimentDataDK.DownloadFromSP()
for key in dictDFs:
    print(key)
    print(dictDFs[key].head(20))