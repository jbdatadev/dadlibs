
import numpy as np
from dadlibs import DivLib as div
from dadlibs import FASLib as fas

d_R = np.random.random(10000)-.5
d_P = np.random.random(10000)
d_P7 = div.Ema(d_P,7)
d_P21 = div.Ema(d_P,21)

dicData = {}
dicData['d_Para'] = [ (d_P21-d_P7)/d_P21 , np.append( np.array([0]), d_R[:-1] ) ]
dicData['d_AC'] = d_R


fas.GenerateSurface(dicData)


dicSett = fas.DefaultSettingsDict()
dicSett['s_DistriTypes'] = ['EqualCount','Normal']
dicSett['i_NumberOfBins'] = [9, 5]
print('Changed default settings to:')
print(dicSett)
dicSurf = fas.GenerateSurface(dicData, dicSett, bPlot = False)
fas.Plot(dicSettings=dicSett, dicSurface=dicSurf)