Collection of Python library functions used in Data Analytics and Development

Add all DAD members as reviewers before merging on main branch (except changes in demoes and tests)

Include in project:
    $ poetry add git+https://bitbucket.org:jbdatadev/dadlibs.git#main