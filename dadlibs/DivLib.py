"""
Lib of misc functions
@author: Anders Haaning
"""
import random
import string
import numpy as np
import yagmail
from pandas.core.common import flatten
from pandas.core.frame import DataFrame

def DefineDateCol(dfData, sDateCol):
    '''
    Define a column as a date index
    '''
    import pandas as pd
    # rename sDateCol to "Date"
    dfData.rename(index=str, columns={sDateCol: "Date"}, inplace=True)
    # transform columns as datetime
    dfData['Date'] = pd.to_datetime(dfData['Date'])
    dfData = dfData.set_index('Date')
    return dfData


def DateRange(oFirstDate, oLastDate):
    '''
    Return a range of dates between dtFirstDate and dfLastDate (both included)
    '''
    import datetime as dt
    dtFirstDate = Date(oFirstDate)
    dtLastDate = Date(oLastDate)
    for iDay in range(int((dtLastDate - dtFirstDate).days) + 1):
        yield dtFirstDate + dt.timedelta(iDay)


def CET2UTC(dt_DateTime, bReturnOffsetAware = False):
    '''
    Convert CET datetime to UTC
    CET is in tz DST aware
    '''
    import pandas as pd
    from dateutil import tz
    if bReturnOffsetAware:
        return dt_DateTime.replace(tzinfo=tz.gettz('CET')).astimezone(tz.gettz('UTC'))
    else:
        if IsSequence(dt_DateTime):
            if isinstance(dt_DateTime, pd.Series):
                return pd.DatetimeIndex(dt_DateTime).tz_localize(tz.gettz('UTC')).tz_convert('CET').to_series(keep_tz=True).apply(lambda d: d.replace(tzinfo=None))
            else:
                Error('NOT IMPLEMENTED')
        else:
            return dt_DateTime.replace(tzinfo=tz.gettz('CET')).astimezone(tz.gettz('UTC')).replace(tzinfo=None)


def UTC2CET(dt_DateTime, bReturnOffsetAware = False):
    '''
    Convert UTC datetime to CET
    CET is in tz DST aware
    '''
    import pandas as pd
    from dateutil import tz

    if bReturnOffsetAware:
        return dt_DateTime.replace(tzinfo=tz.gettz('UTC')).astimezone(tz.gettz('CET'))
    else:
        if IsSequence(dt_DateTime):
            if isinstance(dt_DateTime, pd.Series):
                return pd.DatetimeIndex(dt_DateTime).tz_localize(tz.gettz('UTC')).tz_convert('CET').to_series(keep_tz=True).apply(lambda d: d.replace(tzinfo=None))
            else:
                Error('NOT IMPLEMENTED')
        else:
            return dt_DateTime.replace(tzinfo=tz.gettz('UTC')).astimezone(tz.gettz('CET')).replace(tzinfo=None)


def DKtime2UTC(dt_DateTime, bReturnOffsetAware = False):
    '''
    Convert DK time datetime to UTC
    '''
    import pandas as pd
    from dateutil import tz
    if bReturnOffsetAware:
        return dt_DateTime.replace(tzinfo=tz.gettz("Europe/Copenhagen")).astimezone(tz.gettz('UTC'))
    else:
        if IsSequence(dt_DateTime):
            if isinstance(dt_DateTime, pd.Series):
                return pd.DatetimeIndex(dt_DateTime).tz_localize(tz.gettz('UTC')).tz_convert("Europe/Copenhagen").to_series(keep_tz=True).apply(lambda d: d.replace(tzinfo=None))
            else:
                Error('NOT IMPLEMENTED')
        else:
            return dt_DateTime.replace(tzinfo=tz.gettz("Europe/Copenhagen")).astimezone(tz.gettz('UTC')).replace(tzinfo=None)


def UTC2DKtime(dt_DateTime, bReturnOffsetAware = False):
    '''
    Convert UTC datetime to DK time
    '''
    import pandas as pd
    from dateutil import tz

    if bReturnOffsetAware:
        return dt_DateTime.replace(tzinfo=tz.gettz('UTC')).astimezone(tz.gettz("Europe/Copenhagen"))
    else:
        if IsSequence(dt_DateTime):
            if isinstance(dt_DateTime, pd.Series):
                return pd.DatetimeIndex(dt_DateTime).tz_localize(tz.gettz('UTC')).tz_convert("Europe/Copenhagen").to_series(keep_tz=True).apply(lambda d: d.replace(tzinfo=None))
            else:
                Error('NOT IMPLEMENTED')
        else:
            return dt_DateTime.replace(tzinfo=tz.gettz('UTC')).astimezone(tz.gettz("Europe/Copenhagen")).replace(tzinfo=None)


def LogReturn(d_X,iLength=1):
    '''
    Calc Log returns. 
    On index i is the return from i to i+iLength. 
    Returns numpy vector of same length as X, with zeros on other fields
    '''
    from numpy import log, append, roll
        
    if iLength==0:
        return log(d_X / d_X.shift(iLength))
    if iLength>0:
        return append(log(d_X / roll(d_X,iLength))[iLength:],[0]*min(iLength,len(d_X)))
    if iLength<0:
        return append([0]*min(-iLength,len(d_X)),log(d_X / roll(d_X,iLength))[:iLength])


def MaxDrawDown(seSer):
    '''
    Find MaxDrawDown
    Returns a negative number!
    '''
    try:
        seMax2here = seSer.expanding(min_periods=1).max()
        seDd2here = seSer - seMax2here
        dMaxDrawDown = seDd2here.min()
    except:
        pass
        dMaxDrawDown = 0
    return dMaxDrawDown


def SmallLargeZero(seSer):
    return 1*(seSer > 0) - 1*(seSer < 0)


def PosPart(seSer):
    iCountValues = (sum(seSer > 0) + sum(seSer < 0))
    if iCountValues>0:
        iPosPart = round(100 * (sum(seSer > 0)) / iCountValues)
    else:
        iPosPart = 50
    return iPosPart


def LargerOrEqual(d_Number,d_CompareTo):
    from numpy import isclose
    return isclose(d_Number,d_CompareTo) | (d_Number>d_CompareTo)


def IsSequence(oObj):
    return (not hasattr(oObj, "strip") and hasattr(oObj, "__getitem__") or hasattr(oObj, "__iter__"))


def SendMail(sTo,sFrom,sSubject,sMessage,sMessageType='plain'):
    '''
    Send utf-8 coded mail
    '''
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    import smtplib
    s_To = sTo.split(';')
    for sToSingle in s_To:
        msg = MIMEMultipart()
        msg['From'] = sFrom
        msg['To'] = sToSingle
        msg['Subject'] = sSubject
        if sMessageType == 'plain':
            sMessage = sMessage.replace('<br><br>', '\n\n')
            sMessage = sMessage.replace('<br>', '   \n')
            sMessage = sMessage.replace('<H4>','')
            sMessage = sMessage.replace('</H4>', '')
            body = sMessage
        elif sMessageType == 'html':
            body =  """\
            <html>
                <head></head>
                <body>
                    <p>""" + sMessage + """
                    </p>
                </body>
            </html>
            """
        msg.attach(MIMEText(body, sMessageType))
        server = smtplib.SMTP('mail.dac.local', 25)
        server.ehlo()
        server.starttls()
        server.send_message(msg)
        server.quit()
    return True


def YaSendMail(sGmailUser, sGmailPwd, sTo, sSubject, sMessage, sAttach = None):
    '''
    Send e-mail thru Gmail.
    Input:
        sGmailUser: Username of registered Gmail user
        sGmailPwd: Gmail user password
        sTo: E-mail recievers (split with ;)
        sSubject: E-mail subject
        sMessage: E-mail message
        sAttach: Path of file to attach (None - no attachment)
    '''
    s_To = sTo.split(';')
    for sToSingle in s_To:

        oYag = yagmail.SMTP(sGmailUser, sGmailPwd)
        if not sAttach:
            oYag.send(to=sToSingle, subject=sSubject, contents=sMessage)
        else:
            oYag.send(to=sToSingle, subject=sSubject, contents=sMessage, attachments=sAttach)
    return 'Mails: '+sSubject+' sent via gmail to: '+sTo


# Change color of each axis
def color_y_axis(ax, color):
    '''
    Color your axes
    '''
    for t in ax.get_yticklabels():
        t.set_color(color)
    return None


def raise_window(figname=None):
    import matplotlib.pyplot as plt
    '''
    Raise the plot window for Figure figname to the foreground.  If no argument
    is given, raise the current figure.

    This function will only work with a Qt graphics backend.  It assumes you
    have already executed the command 'import matplotlib.pyplot as plt'.
    '''
    if figname: plt.figure(figname)
    cfm = plt.get_current_fig_manager()
    cfm.window.activateWindow()
    cfm.window.raise_()


def NotOnList(lList):
    return [not oElement for oElement in lList]


def h5store(filename, df, **kwargs):
    import pandas as pd
    store = pd.HDFStore(filename)
    store.put('mydata', df)
    store.get_storer('mydata').attrs.metadata = kwargs
    store.close()


def h5load(store):
    data = store['mydata']
    metadata = store.get_storer('mydata').attrs.metadata
    return data, metadata


def DetrendWeek(dfData, sColName):
    '''
    Week detrend column in dataframe
    '''
    import statistics as st
    dfData['DateTemp']=dfData.index
    for iWeekDayIdx in range(6):
        dfData[sColName][dfData['DateTemp'].dt.weekday==iWeekDayIdx] = dfData[sColName][dfData['DateTemp'].dt.weekday==iWeekDayIdx] - st.median(dfData[sColName][dfData['DateTemp'].dt.weekday==iWeekDayIdx])
    del dfData['DateTemp']
    return dfData


def DetrendYear(dfData, sColName):
    # NOT FINISHED
    import statistics as st
    dfData['DateTemp']=dfData.index
    for iDayIdx in range(6):
        dfData[sColName][dfData['DateTemp'].dt.weekday==iWeekDayIdx] = dfData[sColName][dfData['DateTemp'].dt.weekday==iWeekDayIdx] - st.median(dfData[sColName][dfData['DateTemp'].dt.weekday==iWeekDayIdx])
    del dfData['DateTemp']
    return dfData


def TodayStr():
    '''
    E.g. '2020-02-20'
    '''
    from datetime import datetime
    return datetime.today().strftime('%Y-%m-%d')


def DataFrameReduceMem(df, bAllowFloat16 = True):
    '''
    Iterate through all the columns of a dataframe and modify the data type
        to reduce memory usage.
    '''
    import numpy as np
    import pandas as pd

    for col in df.columns:
        col_type = df[col].dtype
        if str(col_type) == 'category':
            pass
        if col_type == object:
            df[col] = df[col].apply(pd.to_numeric, errors='ignore')
        if col_type != object:
            c_min = df[col].min()
            c_max = df[col].max()
            if str(col_type)[:3] == 'int':
                if c_min > np.iinfo(np.int8).min and c_max < np.iinfo(np.int8).max:
                    df[col] = df[col].astype(np.int8)
                elif c_min > np.iinfo(np.int16).min and c_max < np.iinfo(np.int16).max:
                    df[col] = df[col].astype(np.int16)
                elif c_min > np.iinfo(np.int32).min and c_max < np.iinfo(np.int32).max:
                    df[col] = df[col].astype(np.int32)
                elif c_min > np.iinfo(np.int64).min and c_max < np.iinfo(np.int64).max:
                    df[col] = df[col].astype(np.int64)
            else:
                if bAllowFloat16 and c_min > np.finfo(np.float16).min and c_max < np.finfo(np.float16).max:
                    df[col] = df[col].astype(np.float16)
                elif c_min > np.finfo(np.float32).min and c_max < np.finfo(np.float32).max:
                    df[col] = df[col].astype(np.float32)
                else:
                    df[col] = df[col].astype(np.float64)
        else:
            df[col] = df[col].astype('category')
    return df


def Error(sMessage):
    import sys
    print('ERROR - '+sMessage)
    sys.exit()


def StrLen2(iNumber):
    '''
    E.g. 4 -> '04'
    '''
    s = str(iNumber)
    if len(s) == 1:
        s = '0'+s
    return s


def Ema(d_Vec, iL):
    '''
    Calc exponential moving average
    '''
    import numpy as np
    d_Ema = np.empty_like(d_Vec)
    d_Ema[0] = d_Vec[0]
    for idx in range(1, len(d_Vec)):
        if np.isnan(d_Vec[idx]):
            d_Ema[idx] = np.nan
        elif np.isnan(d_Ema[idx-1]):
            d_Ema[idx] = d_Vec[idx]
        else:
            d_Ema[idx] = (d_Ema[idx-1]*(iL-1) + d_Vec[idx])/iL
    return d_Ema


def Date(oDate, bReturnAsDatetime = False, bReturnAsString = False):
    '''
    Return string, date, datetime, pd.Timestamp or np.datetime64 as date
    Or if bReturnAsDatetime as datetime
    If bReturnAsString as '%Y-%m-%d' string
    '''
    from datetime import datetime
    from datetime import date
    import pandas as pd
    if type(oDate) is str:
        if len(oDate)==10:
            dtDate = datetime.strptime(oDate, r'%Y-%m-%d').date()
        else:
            dtDate = datetime.strptime(oDate, r'%Y-%m-%d %H:%M:%S').date()
    elif type(oDate) is date:
        dtDate = oDate
    elif type(oDate) is datetime:
        dtDate = oDate.date()
    elif type(oDate) is pd.Timestamp:
        dtDate = oDate.to_pydatetime().date()
    elif type(oDate) is np.datetime64:
        dtDate = datetime.strptime(np.datetime_as_string(oDate,unit='s'), r'%Y-%m-%dT%H:%M:%S')
    else:
        raise TypeError('oDate must be string, date, datetime, pd.Timestamp or np.datetime64 - not: '+str(type(oDate)))
    if bReturnAsDatetime:
        return datetime(dtDate.year, dtDate.month, dtDate.day)
    elif bReturnAsString:
        return dtDate.strftime('%Y-%m-%d')
    else:
        return dtDate


def Date2Datetime(d):
    '''
    Convert date to datetime
    '''
    from datetime import datetime
    return datetime(d.year, d.month, d.day)


def List2Str(lList,sSep=''):
    '''
    Convert list to string containing list elements
    '''
    return sSep.join([str(oElem) for oElem in lList])


def RunningOS():
    '''
    Return current operating system (Linux or Windows)
    '''
    import os
    try:
        os.uname()
        return 'Linux'
    except AttributeError:
        return 'Windows'


def CusipPad9thDigit(sCusip):
    '''
    Pad the 9th digit on a 8 digit CUSIP
    '''
    sCusip = str(sCusip)
    if not (set(sCusip) <= set('0123456789ABCDEFGH JKLMN PQRSTUVWXYZ*@#')):
        return 'NOTVALID'
    if (len(sCusip)<=7)or(len(sCusip)>=10):
        return 'NOTVALID'
    if len(sCusip)==9:
        return sCusip
    sTemp = ''.join(str((1, 2)[i % 2] * '0123456789ABCDEFGH JKLMN PQRSTUVWXYZ*@#'.index(n)) for i, n in enumerate(sCusip))
    return sCusip + str((10 - sum(int(n) for n in sTemp)) % 10)


def CusipListPad9thDigit(lCusips, bKeepNotValid = True):
    '''
    Pad the 9th digit on a list of 8 digit CUSIPs
    '''
    lCusipsPadded = list(map(CusipPad9thDigit, lCusips))
    if not bKeepNotValid:
        lCusipsPadded = list(filter(lambda sCusip: sCusip !='NOTVALID', lCusipsPadded))
    return lCusipsPadded


def NumpyNanMean(d_Set, dReturnIfAllNaN = np.NaN):
    '''
    Calc mean while ignore NaNs
    '''
    return dReturnIfAllNaN if np.all(d_Set!=d_Set) else np.nanmean(d_Set)


def BlomNormalize(oData, lModelCols = None):
    '''
    Blom Normalize
    NOT TESTED
    '''
    import pandas as pd
    from scipy.stats import norm
    if type(oData) is DataFrame:
        dfRanks = oData[lModelCols].rank(axis=0,method="average",na_option="keep")
        return pd.DataFrame(norm.ppf((dfRanks.values-(3/8))/(pd.Series(dfRanks.count()).values+(1/4))),columns=dfRanks.columns)
    else:
        lRanks = oData.rank(axis=0,method="average",na_option="keep")
        return norm.ppf((lRanks-(3/8))/(lRanks.count()+(1/4)))
    
    
def RandomString(iLength=7, sPrefix='JB_'):
    '''
    Return a random string
    Input:
        iLength: Length of string (without prefix)
        sPrefix: Start the returned string with this prefix
    '''
    return sPrefix + ''.join(random.choices(string.ascii_uppercase + string.digits, k = iLength)) 


def ReplaceInList(lList, oOld, oNew):
    '''
    Replace elements in list
    '''
    if oOld is np.nan:
        return [oNew if (oValue is np.nan) else oValue for oValue in lList]
    else:
        return [oNew if oValue==oOld else oValue for oValue in lList]
    
    
def SaveListToFile(lList, sFile):
    '''
    Save a list to a file
    Input:
        lList: List to save
        sFile: Full file path
    '''
    with open(sFile, 'w') as oFilehandle:
        oFilehandle.writelines("%s\n" % oItem for oItem in lList)
        
        
def AllKeys(lListOfDicts, bUniqUnOrdered = False):
    '''
    Return all keys in a list of dicts
    Input:
        lListOfDicts: A list of dicts
        bUniqUnOrdered: Return only unique keys (unordered)
    '''
    if bUniqUnOrdered:
        return list(set().union(*(dicKeyValue.keys() for dicKeyValue in lListOfDicts)))
    else:
        return [iKey for oKey in [dicKeyValue.keys() for dicKeyValue in lListOfDicts] for iKey in oKey]


def AllValues(lListOfDicts, bUniqUnOrdered = False, bFlatten = False):
    '''
    Return all values in a list of dicts
    e.g. s_C25sedol = rpdata.AllValues( oRP.Isins2Sedols(s_C25isin)[0], bFlatten= True )
    Input:
        lListOfDicts: A list of dicts
        bUniqUnOrdered: Return only unique values (unordered)
    '''
    if bUniqUnOrdered:
        lReturn = list(set().union(*(dicKeyValue.values() for dicKeyValue in lListOfDicts)))
    else:
        lReturn = [iValue for oValue in [dicKeyValue.values() for dicKeyValue in lListOfDicts] for iValue in oValue]
    if bFlatten:
        return list(flatten(lReturn))
    else:
        return lReturn