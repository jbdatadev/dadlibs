"""

The idea of the FASLib is to make statistics of PnL as function of
one, two or three data parameters in a defined number of bins
The bin sizes are found from a distribution defined for each parameter

@author: Anders Haaning
"""

import numpy as np
import numbers
import warnings
from dadlibs import DivLib as div


def SurfaceMean(dicSurface):
    return np.nanmean( dicSurface['Means'] )

def DefaultSettingsDict():
    """
    This function returns a dict containing default settings for FAS surface generation
    Returns:
        dicSettings: Settings.
            ['iDimensionality']: desired dimensionality of surface - if None use dimensionality of dicData['d_Para']
            ['iProfitLength']: if iProfitLength is 0 then d_PriceOrReturn is the return else d_PriceOrReturn is the price and the return is calculated iProfitLength steps into the future
            ['bCutOutliers']: cut outliers in the return serie. Cut to border value
            ['dOutlierBorderStd']: if bCutOutliers cut outliers to this value
            ['s_DistriTypes']: choose between 'EquallySpaced', 'Normal' or 'EqualCount'
            ['i_NumberOfBins']: the number of bins in each direction. Must have at least iDimensionality elements
            ['iMinCount']: if the count in a bin is below iMinCount then set the bin value in SmoothMeans to NaN
            ['iSmoothStrength']: if >0 apply Gauss filter with sigma = iSmoothStrength to SmoothMeans
            ['dic_BinNames']: (optional) Names for the bins e.g. [[], ['Q1','Q2','Q3','Q4'], []]
    """
    return {
        'iDimensionality': None,
        'iProfitLength': 0,
        'bCutOutliers': True,
        'dOutlierBorderStd': 2.0,
        's_DistriTypes': ['EqualCount','EqualCount','EqualCount'],
        'i_NumberOfBins': [9, 9, 9],
        'iMinCount': 5,
        'iSmoothStrength': 1,
        'sSerieName': 'Default Serie Name',
        's_ParaNames': ['P1','P2','P3']
        }


def BinValue(dicSurface, d_ParaVec):
    """
    Uses parameter values to lookup the value in the corresponding bin
    Args:
        dicSurface: A FAS surface from one of the ProfitInBin functions
        d_ParaVec: A vector with parameter values
    Returns:
        The dicSurface bin value for the parameters in d_Para
        If the parameters does'nt is outside surface borders nan is returned
    """
    iSurfaceDim = dicSurface['Dimensionality']
    if iSurfaceDim == len(d_ParaVec):
        d_BinBorders = dicSurface['BinBorders']
        i_Idx = np.zeros(iSurfaceDim)

        for iDim in range(iSurfaceDim):
            if iSurfaceDim == 1:
                d_DimBorders = d_BinBorders
            else:
                d_DimBorders = d_BinBorders[iDim,]
            dDimPara = d_ParaVec[iDim]
            if not 0 < np.searchsorted(d_DimBorders, dDimPara) < len(d_DimBorders):
                return np.nan
            i_Idx[iDim] = np.searchsorted(d_DimBorders, dDimPara) - 1

        return dicSurface['SmoothMeans'][tuple(i_Idx.astype(int))]
    else:
        return np.nan


def BinBorders(d_ParaVec, sDistriType, iNumberOfBins):
    """
    Find the borders of the bins for a given distribution and a given number of bins
    Args:
        d_ParaVec: Vector with parameter values
        sDistriType: 'EquallySpaced', 'Normal' or 'EqualCount'
        i_NumberOfBins: The number of bins
    Returns:
        The bin borders: Vector with iNumberOfBins+1 elements
    """
    import scipy.stats as st
    if sDistriType == 'EquallySpaced':
        d_BinBorders = np.linspace(np.min(d_ParaVec), np.max(d_ParaVec), iNumberOfBins + 1)
    elif sDistriType == 'Normal':
        d_BordersStd = st.norm.ppf(np.hstack([0.01, np.linspace(1 / iNumberOfBins, 1, iNumberOfBins - 1, False), 0.99]))
        d_BinBorders = np.mean(d_ParaVec) + np.multiply(np.std(d_ParaVec), d_BordersStd)
    elif sDistriType == 'EqualCount':
        d_BinBorders = np.percentile(d_ParaVec, np.linspace(0, 100, iNumberOfBins + 1))
    else:
        raise LookupError('Unknown distribution type: ' + sDistriType)
    return d_BinBorders.astype(np.float32)



# def GenerateFromFASSettings(dicSettings, bPlot=False, sFileName=''):
#     if not 'sSerieName' in dicSettings:
#         dicSettings['sSerieName'] = 'PnL ' + dicSettings['sPriceArea'] + ' ' + dicSettings['sStartDateTime'] + '_' + \
#                                     dicSettings['sEndDateTime']
#     dicData = DataFromFASSettings(dicSettings)
#     dicSurface = GenerateSurface(dicData, dicSettings, bPlot, sFileName)
#     return dicSettings, dicSurface


def SaveFASjson(sFileName, dicSettings, dicSurface):
    """
    Save settings and surface to a json file
    Args:
        sFileName: json file with path
        dicSettings: Settings used for generating the surface
        dicSurface: FAS surface
    Returns:
        Nothing
    """
    import json_tricks as jnp

    dicSettingsCopy = dicSettings.copy()
    dicSettingsCopy["s_ParaNames"] = dicSettingsCopy["s_ParaNames"][:dicSurface['Dimensionality']]
    dicSettingsCopy["s_DistriTypes"] = dicSettingsCopy["s_DistriTypes"][:dicSurface['Dimensionality']]
    dicSettingsCopy["i_NumberOfBins"] = dicSettingsCopy["i_NumberOfBins"][:dicSurface['Dimensionality']]

    oDumpFile = open(sFileName, "w")
    jnp.dump({'Settings': dicSettingsCopy, 'Surface': dicSurface}, oDumpFile, allow_nan=True)
    oDumpFile.close()


def LoadFASjson(sFileName):
    """
    Load a FAS surface and settings from json file
    Args:
        sFileName: json file with path
    Returns:
        dicSettings: Settings used for generating the surface
        dicSurface: FAS surface
    """
    import json_tricks as jnp

    oDumpFile = open(sFileName, "r")
    oJson = jnp.load(oDumpFile)
    oDumpFile.close()
    dicSettings = oJson['Settings']
    if 'Surface' in oJson:
        dicSurface = oJson['Surface']
    else:
        dicSurface = {}
    return dicSettings, dicSurface


def BinTicks(iXYZ, dicSettings, dicSurface):
    """
    If there is named bins generate the ticks for plots
    Args:
        iXYZ: Axis index (0 to 2)
        dicSettings: Settings used for generating the surface
        dicSurface: FAS surface
    Returns:
        d_Tick: Place of the ticks
        s_Tick: Bin names
    """
    if 'dic_BinNames' in dicSettings:
        if len(dicSettings['dic_BinNames'][iXYZ])>0:
            s_Tick = dicSettings['dic_BinNames'][iXYZ]
            d_Tick = np.arange(dicSettings['i_NumberOfBins'][iXYZ])
            return d_Tick, s_Tick
    if not isinstance(dicSurface['BinBorders'][0], numbers.Number):
        return np.arange(-0.5, dicSettings['i_NumberOfBins'][iXYZ], 1), dicSurface['BinBorders'][iXYZ].astype(float).round(2)
    else:
        return np.arange(-0.5, dicSettings['i_NumberOfBins'][iXYZ], 1), dicSurface['BinBorders'].astype(float).round(2)


def PlotFASjson(sFileName):
    """
    Plot a FAS surface from json file
    Args:
        sFileName: json file with path
    Returns:
        Nothing
    """
    dicSettings, dicSurface = LoadFASjson(sFileName)
    Plot(dicSettings, dicSurface)


def Plot(dicSettings, dicSurface):
    """
    Plot a FAS surface. Calls functions based on the dimensionality of the surface
    Args:
        dicSettings: Settings used for generating the surface
        dicSurface: FAS surface
    Returns:
        Nothing
    """
    if dicSurface['Dimensionality'] == 1:
        Plot1d(dicSettings, dicSurface)
    elif dicSurface['Dimensionality'] == 2:
        Plot2d(dicSettings, dicSurface)
    elif dicSurface['Dimensionality'] == 3:
        Plot3d(dicSettings, dicSurface)
    else:
        print('Problem with json file')


def Plot1d(dicSettings, dicSurface, dMeanP1=np.nan, dMeanProfit=np.nan):
    """
    Plot a 1d FAS surface. Calls functions based on the dimensionality of the surface
    Args:
        dicSettings: Settings used for generating the surface
        dicSurface: FAS surface
        dMeanP1: If provided it is shown in the title
        dMeanProfit: If provided it is shown in the title
    Returns:
        Nothing
    """
    import matplotlib as ml
    # ml.use('Qt5Agg')
    import matplotlib.pyplot as plt
    from matplotlib.ticker import MaxNLocator

    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    plt.subplots_adjust(right=0.7)
    ax1.plot(dicSurface['SmoothMeans'].T, 'b-', lw=3, label='SmoothMeans')
    ax1.plot(dicSurface['Means'].T, 'y-', label='Means')
    ax1.plot(dicSurface['Median'].T, 'c-', label='Median')
    ax1.plot(dicSurface['MeansInclOutliers'].T, 'm-', label='MeansInclOutliers')
    ax2 = ax1.twinx()
    ax2.plot(dicSurface['Count'].T, 'g.', label='Count')
    ax2.yaxis.set_major_locator(MaxNLocator(integer=True))
    ax3 = ax1.twinx()
    ax3.spines['right'].set_position(('outward', 40))
    ax3.plot(dicSurface['StDev'].T, 'r.', label='StDev')
    ax4 = ax1.twinx()
    ax4.spines['right'].set_position(('outward', 80))
    ax4.plot(dicSurface['PosiPart'].T, 'b:', label='PosiPart')
    if np.isnan(dMeanProfit):
        ax1.set_ylabel('Mean value in bin', color='b')
    else:
        ax1.set_ylabel('Mean value in bin (Overall mean: ' + str(round(dMeanProfit, 3)) + ')', color='b')
    ax2.set_ylabel('Count in bin (Total: ' + str(int(sum(dicSurface['Count']))) + ')', color='g')
    ax3.set_ylabel('StDev in bin', color='r')
    ax4.set_ylabel('PosiPart in bin', color='b')
    ax1.grid(color='b', linestyle=':', linewidth=.5)
    d_Xtick, s_Xtick = BinTicks(0, dicSettings, dicSurface)
    plt.xticks(d_Xtick, s_Xtick, rotation=90)
    if np.isnan(dMeanP1):
        ax1.set_xlabel(dicSettings['s_ParaNames'][0])
    else:
        ax1.set_xlabel(dicSettings['s_ParaNames'][0] + ' (Mean: ' + str(round(dMeanP1, 3)) + ')')
    plt.title(dicSettings['sSerieName'])
    ax1.legend()
    plt.show()


def Plot2d(dicSettings, dicSurface):
    """
    Plot a 2d FAS surface. Calls functions based on the dimensionality of the surface
    Args:
        dicSettings: Settings used for generating the surface
        dicSurface: FAS surface
    Returns:
        Nothing
    """
    import matplotlib as ml
    # ml.use('Qt5Agg')
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm

    d_BinSmoothMeans = dicSurface['SmoothMeans']
    d_BinMeans = dicSurface['Means']
    i_BinCount = dicSurface['Count']

    d_Xtick, s_Xtick = BinTicks(0, dicSettings, dicSurface)
    d_Ytick, s_Ytick = BinTicks(1, dicSettings, dicSurface)

    plt.figure()
    plt.imshow(d_BinSmoothMeans.T, interpolation='bilinear', cmap=cm.RdYlGn, origin='lower',
               vmax=np.nanmax(abs(d_BinSmoothMeans)),
               vmin=-np.nanmax(abs(d_BinSmoothMeans)))  # nearest
    plt.colorbar()
    plt.xlabel(dicSettings['s_ParaNames'][0])
    plt.ylabel(dicSettings['s_ParaNames'][1])
    plt.title(dicSettings['sSerieName'] + ' Smooth means')
    plt.xticks(d_Xtick, s_Xtick, rotation=90)
    plt.yticks(d_Ytick, s_Ytick)
    plt.show(block=True)

    plt.figure()
    plt.imshow(d_BinMeans.T, interpolation='nearest', cmap=cm.RdYlGn, origin='lower', vmax=np.nanmax(abs(d_BinMeans)),
               vmin=-np.nanmax(abs(d_BinMeans)))  # nearest
    plt.colorbar()
    plt.xlabel(dicSettings['s_ParaNames'][0])
    plt.ylabel(dicSettings['s_ParaNames'][1])
    plt.title(dicSettings['sSerieName'] + ' Means')
    plt.xticks(d_Xtick, s_Xtick, rotation=90)
    plt.yticks(d_Ytick, s_Ytick)
    plt.show(block=True)

    plt.figure()
    plt.imshow(i_BinCount.T, interpolation='nearest', cmap=cm.RdYlGn, origin='lower', vmax=abs(i_BinCount).max(),
               vmin=0)  # nearest
    plt.colorbar()
    plt.xlabel(dicSettings['s_ParaNames'][0])
    plt.ylabel(dicSettings['s_ParaNames'][1])
    plt.title(dicSettings['sSerieName'] + ' Count - Total: ' + str(int(sum(i_BinCount.flatten()))))
    plt.xticks(d_Xtick, s_Xtick, rotation=90)
    plt.yticks(d_Ytick, s_Ytick)
    plt.show(block=True)


def Plot3d(dicSettings, dicSurface):
    """
    Plot a 3d FAS surface. Calls functions based on the dimensionality of the surface
    Args:
        dicSettings: Settings used for generating the surface
        dicSurface: FAS surface
    Returns:
        Nothing
    """
    import matplotlib as ml
    # ml.use('Qt5Agg')
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm
    from mpl_toolkits.mplot3d import Axes3D  # must be there

    d_BinMeans = dicSurface['SmoothMeans']
    i_BinCount = dicSurface['Count']

    d_Xtick, s_Xtick = BinTicks(0, dicSettings, dicSurface)
    d_Ytick, s_Ytick = BinTicks(1, dicSettings, dicSurface)
    d_Ztick, s_Ztick = BinTicks(2, dicSettings, dicSurface)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    d_FlatMean = d_BinMeans.flatten()
    i_FlatCount = i_BinCount.flatten()
    cmap = cm.get_cmap('RdYlGn')
    if np.all(np.isnan(d_FlatMean)):
        normalize = ml.colors.Normalize(vmin=-1, vmax=1)
    else:
        normalize = ml.colors.Normalize(vmin=np.nanmin(d_FlatMean), vmax=np.nanmax(d_FlatMean))
        i_FlatCount[np.isnan(d_FlatMean)] = 0
    np.nan_to_num(d_FlatMean, False)
    colors = [cmap(normalize(value)) for value in d_FlatMean]
    sizes = (3000 * i_FlatCount / np.mean(i_FlatCount)) + 0.0001 * np.ones_like(i_FlatCount)
    x, y, z = np.meshgrid(range(dicSettings['i_NumberOfBins'][0]), range(dicSettings['i_NumberOfBins'][1]),
                          range(dicSettings['i_NumberOfBins'][2]))
    ax.scatter(x, y, z, c=colors, s=sizes)
    cax, _ = ml.colorbar.make_axes(ax)
    cbar = ml.colorbar.ColorbarBase(cax, cmap=cmap, norm=normalize)
    plt.title(dicSettings['sSerieName'])
    ax.set_xticks(d_Xtick)
    ax.set_yticks(d_Ytick)
    ax.set_zticks(d_Ztick)
    ax.set_xticklabels(s_Xtick)
    ax.set_yticklabels(s_Ytick)
    ax.set_zticklabels(s_Ztick)
    ax.set_xlabel(dicSettings['s_ParaNames'][0])
    ax.set_ylabel(dicSettings['s_ParaNames'][1])
    ax.set_zlabel(dicSettings['s_ParaNames'][2])
    plt.show()


def ProfitInBin1d(dicData, dicSettings, bPlot=True, sFileName=''):
    """
    Generates a 1d FAS surface.  Mean PnL as function of data parameters in a defined number of bins with bin size from a distribution
    Args:
        dicData: Data for the analysis. Must have dicData['d_AC'] and dicData['d_Para']
            ['d_AC']: the returns of the analysed asset if dicSettings['iProfitLength'] == 0 or otherwise the price of the asset
            ['d_Para']: ['d_Para'][0] is the parameter that the asset may depend on
        dicSettings: Settings.
            ['iProfitLength']: if iProfitLength is 0 then d_PriceOrReturn is the return else d_PriceOrReturn is the price and the return is calculated iProfitLength steps into the future
            ['bCutOutliers']: cut outliers in the return serie. Cut to border value
            ['dOutlierBorderStd']: if bCutOutliers cut outliers to this value
            ['s_DistriType']: choose between 'EquallySpaced', 'Normal' or 'EqualCount'
            ['i_NumberOfBins']: ['i_NumberOfBins'][0] is the number of bins in the surface
            ['iMinCount']: if the count in a bin is below iMinCount then set the bin value in SmoothMeans to NaN
            ['iSmoothStrength']: if >0 apply Gauss filter with sigma = iSmoothStrength to SmoothMeans
            ['dic_BinNames']: (optional) Names for the bins e.g. [[], ['Q1','Q2','Q3','Q4']]
        bPlot: Plot the surface (in this 1d case the line)
            if bPlot dicSetting must include:
                dicSettings['sSerieName']
                dicSettings['s_ParaNames']
        sFileName: If set then save the settings and the surface to a json file

    Returns:
        The function returns a 1d FAS surface in a dictionary.
        dicSurface = {}
        dicSurface['Dimensionality'] = 1
        dicSurface['BinBorders']: the borders of the single bins. i_NumberOfBins + 1 elements
        dicSurface['SmoothMeans']: the smoothed mean value in each bin. nan if count < iMinCount. i_NumberOfBins elements
        dicSurface['Count']: the number of events in each bin. i_NumberOfBins elements
        dicSurface['Means']: the mean value in each bin. i_NumberOfBins elements
        dicSurface['Median']: the meadian value in each bin. i_NumberOfBins elements
        dicSurface['MeansInclOutliers']: the mean value including outliers in each bin. i_NumberOfBins elements
        dicSurface['StDev']: the mean value in each bin. i_NumberOfBins elements
        dicSurface['PosiPart']: the mean value in each bin. i_NumberOfBins elements
    """

    b_NotNans = np.logical_not(np.isnan(dicData['d_AC']) + np.isnan(dicData['d_Para'][0]))
    d_AC = dicData['d_AC'][b_NotNans]
    d_Para = dicData['d_Para'][0][b_NotNans]

    if dicSettings['iProfitLength'] == 0:
        d_ProfitInclOutliers = d_AC
    else:
        d_ProfitInclOutliers = div.LogReturn(d_AC, dicSettings['iProfitLength'])

    if dicSettings['bCutOutliers']:
        d_Profit = np.clip(d_ProfitInclOutliers, -dicSettings['dOutlierBorderStd'] * np.std(d_ProfitInclOutliers),
                           dicSettings['dOutlierBorderStd'] * np.std(d_ProfitInclOutliers))
    else:
        d_Profit = d_ProfitInclOutliers

    d_Para = np.random.normal(d_Para, 0.001 * np.std(d_Para))  # helps if the distribution has often recurring values

    d_BinBorders = BinBorders(d_Para, dicSettings['s_DistriTypes'][0], dicSettings['i_NumberOfBins'][0], dtype=object)
    d_BinTotalPnL = np.full(dicSettings['i_NumberOfBins'][0], np.nan)
    d_SmoothBinMeans = np.full(dicSettings['i_NumberOfBins'][0], np.nan)
    d_BinMeans = np.full(dicSettings['i_NumberOfBins'][0], np.nan)
    d_BinMedian = np.full(dicSettings['i_NumberOfBins'][0], np.nan)
    d_BinStDev = np.full(dicSettings['i_NumberOfBins'][0], np.nan)
    d_BinMeansInclOutliers = np.full(dicSettings['i_NumberOfBins'][0], np.nan)
    d_PosiPart = np.full(dicSettings['i_NumberOfBins'][0], np.nan)
    i_BinCount = np.zeros(dicSettings['i_NumberOfBins'][0])

    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        for iX in range(dicSettings['i_NumberOfBins'][0]):
            b_InRange = (d_BinBorders[iX] <= d_Para) & (d_Para < d_BinBorders[iX + 1])
            i_BinCount[iX] = sum(b_InRange)

            d_ProfitInRange = d_Profit[b_InRange]
            d_BinTotalPnL[iX] = np.sum(d_ProfitInclOutliers[b_InRange])
            d_BinMeans[iX] = np.mean(d_ProfitInRange)
            d_BinMedian[iX] = np.nanmedian(d_ProfitInRange)
            d_BinStDev[iX] = np.std(d_ProfitInRange)
            d_BinMeansInclOutliers[iX] = np.mean(d_ProfitInclOutliers[b_InRange])
            if sum(d_ProfitInRange != 0) > 0:
                d_PosiPart[iX] = sum(d_ProfitInRange > 0) / sum(d_ProfitInRange != 0)

            if i_BinCount[iX] > dicSettings['iMinCount']:
                d_SmoothBinMeans[iX] = np.mean(d_ProfitInRange)

        if dicSettings['iSmoothStrength'] > 0:
            from scipy.ndimage.filters import gaussian_filter
            d_Temp = np.zeros(dicSettings['i_NumberOfBins'][0] + 2)  # Avoid the borders to dominate
            d_Temp[1:-1] = d_SmoothBinMeans
            d_Temp = gaussian_filter(d_Temp, sigma=dicSettings['iSmoothStrength'])
            d_SmoothBinMeans = d_Temp[1:-1]

    dicSurface = {}
    dicSurface['Dimensionality'] = 1
    dicSurface['BinBorders'] = d_BinBorders.astype(np.float32)
    dicSurface['PnL'] = d_BinTotalPnL.astype(np.float32)
    dicSurface['SmoothMeans'] = d_SmoothBinMeans.astype(np.float32)
    dicSurface['Means'] = d_BinMeans.astype(np.float32)
    dicSurface['Median'] = d_BinMedian.astype(np.float32)
    dicSurface['StDev'] = d_BinStDev.astype(np.float32)
    dicSurface['MeansInclOutliers'] = d_BinMeansInclOutliers.astype(np.float32)
    dicSurface['PosiPart'] = d_PosiPart.astype(np.float32)
    dicSurface['Count'] = i_BinCount.astype(int)

    if bPlot:
        Plot1d(dicSettings, dicSurface, np.mean(d_Para), np.mean(d_Profit))

    if not sFileName == '':
        SaveFASjson(sFileName, dicSettings, dicSurface)
    return dicSurface


def ProfitInBin2d(dicData, dicSettings, bPlot=True, sFileName=''):
    """
        Generates a 2d FAS surface.  Mean PnL as function of data parameters in a defined number of bins with bin size from a distribution
        Args:
            dicData: Data for the analysis. Must have dicData['d_AC'], dicData['d_Para']
                ['d_AC']: the returns of the analysed asset if dicSettings['iProfitLength'] == 0 or otherwise the price of the asset
                ['d_Para']: the parameters that the asset may depend on
            dicSettings: Settings.
                ['iProfitLength']: if iProfitLength is 0 then d_PriceOrReturn is the return else d_PriceOrReturn is the price and the return is calculated iProfitLength steps into the future
                ['bCutOutliers']: cut outliers in the return serie. Cut to border value
                ['dOutlierBorderStd']: if bCutOutliers cut outliers to this value
                ['s_DistriTypes']: choose between 'EquallySpaced', 'Normal' or 'EqualCount'
                ['i_NumberOfBins']: the number of bins in each direction. Must have at least two elements
                ['iMinCount']: if the count in a bin is below iMinCount then set the bin value in SmoothMeans to NaN
                ['iSmoothStrength']: if >0 apply Gauss filter with sigma = iSmoothStrength to SmoothMeans
                ['dic_BinNames']: (optional) Names for the bins e.g. [[], ['Q1','Q2','Q3','Q4']]
            bPlot: Plot the surface and the event counts
                if bPlot dicSetting must include:
                    dicSettings['sSerieName']
                    dicSettings['s_ParaNames']
            sFileName: If set then save the settings and the surface to a json file
        Returns:
            The function returns a 2d FAS surface in a dictionary.
            dicSurface = {}
            dicSurface['Dimensionality'] = 2
            dicSurface['BinBorders']: the borders of the single bins. i_NumberOfBins + 1 elements
            dicSurface['SmoothMeans']: the smoothed mean value in each bin. nan if count < iMinCount. i_NumberOfBins elements
            dicSurface['Count']: the number of events in each bin. i_NumberOfBins elements
            dicSurface['Means']: the mean value in each bin. i_NumberOfBins elements
            dicSurface['Median']: the meadian value in each bin. i_NumberOfBins elements
            dicSurface['MeansInclOutliers']: the mean value including outliers in each bin. i_NumberOfBins elements
            dicSurface['StDev']: the mean value in each bin. i_NumberOfBins elements
            dicSurface['PosiPart']: the mean value in each bin. i_NumberOfBins elements
        """

    from scipy.ndimage.filters import gaussian_filter

    b_NotNans = np.logical_not(np.isnan(dicData['d_AC']) + np.isnan(dicData['d_Para'][0]) + np.isnan(dicData['d_Para'][1]))
    d_AC = dicData['d_AC'][b_NotNans]
    d_P1 = dicData['d_Para'][0][b_NotNans]
    d_P2 = dicData['d_Para'][1][b_NotNans]

    if dicSettings['iProfitLength'] == 0:
        d_ProfitInclOutliers = d_AC
    else:
        d_ProfitInclOutliers = div.LogReturn(d_AC, dicSettings['iProfitLength'])

    if dicSettings['bCutOutliers']:
        d_Profit = np.clip(d_ProfitInclOutliers, -dicSettings['dOutlierBorderStd'] * np.std(d_ProfitInclOutliers),
                           dicSettings['dOutlierBorderStd'] * np.std(d_ProfitInclOutliers))
    else:
        d_Profit = d_ProfitInclOutliers

    d_P1 = np.random.normal(d_P1, 0.001 * np.std(d_P1))  # helps if the distribution has often recurring values
    d_P2 = np.random.normal(d_P2, 0.001 * np.std(d_P2))
    d_BinBorders = np.array([BinBorders(d_P1, dicSettings['s_DistriTypes'][0], dicSettings['i_NumberOfBins'][0]),
                             BinBorders(d_P2, dicSettings['s_DistriTypes'][1], dicSettings['i_NumberOfBins'][1])], dtype=object)
    d_BinTotalPnL = np.full([dicSettings['i_NumberOfBins'][0], dicSettings['i_NumberOfBins'][1]], np.nan)
    d_SmoothBinMeans = np.full([dicSettings['i_NumberOfBins'][0], dicSettings['i_NumberOfBins'][1]], np.nan)
    d_BinMeans = np.full([dicSettings['i_NumberOfBins'][0], dicSettings['i_NumberOfBins'][1]], np.nan)
    d_BinMedian = np.full([dicSettings['i_NumberOfBins'][0], dicSettings['i_NumberOfBins'][1]], np.nan)
    d_BinStDev = np.full([dicSettings['i_NumberOfBins'][0], dicSettings['i_NumberOfBins'][1]], np.nan)
    d_BinMeansInclOutliers = np.full([dicSettings['i_NumberOfBins'][0], dicSettings['i_NumberOfBins'][1]], np.nan)
    d_PosiPart = np.full([dicSettings['i_NumberOfBins'][0], dicSettings['i_NumberOfBins'][1]], np.nan)
    i_BinCount = np.zeros([dicSettings['i_NumberOfBins'][0], dicSettings['i_NumberOfBins'][1]])

    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        for iX in range(dicSettings['i_NumberOfBins'][0]):
            for iY in range(dicSettings['i_NumberOfBins'][1]):
                b_InRange = (d_BinBorders[0][iX] <= d_P1) & (d_P1 < d_BinBorders[0][iX + 1]) & (
                            d_BinBorders[1][iY] <= d_P2) & (d_P2 < d_BinBorders[1][iY + 1])
                i_BinCount[iX, iY] = sum(b_InRange)

                d_ProfitInRange = d_Profit[b_InRange]
                d_BinTotalPnL[iX, iY] = np.sum(d_ProfitInclOutliers[b_InRange])
                d_BinMeans[iX, iY] = np.mean(d_ProfitInRange)
                d_BinMedian[iX, iY] = np.nanmedian(d_ProfitInRange)
                d_BinStDev[iX, iY] = np.std(d_ProfitInRange)
                d_BinMeansInclOutliers[iX, iY] = np.mean(d_ProfitInclOutliers[b_InRange])
                if sum(d_ProfitInRange != 0) > 0:
                    d_PosiPart[iX, iY] = sum(d_ProfitInRange > 0) / sum(d_ProfitInRange != 0)

                if i_BinCount[iX, iY] > dicSettings['iMinCount']:
                    d_SmoothBinMeans[iX, iY] = np.mean(d_ProfitInRange)

        if dicSettings['iSmoothStrength'] > 0:
            d_Temp = np.zeros((dicSettings['i_NumberOfBins'][0] + 2,
                               dicSettings['i_NumberOfBins'][1] + 2))  # Avoid the borders to dominate
            d_Temp[1:-1, 1:-1] = d_SmoothBinMeans
            d_Temp = gaussian_filter(d_Temp, sigma=dicSettings['iSmoothStrength'])
            d_SmoothBinMeans = d_Temp[1:-1, 1:-1]

    dicSurface = {}
    dicSurface['Dimensionality'] = 2
    dicSurface['BinBorders'] = d_BinBorders
    dicSurface['PnL'] = d_BinTotalPnL.astype(np.float32)
    dicSurface['SmoothMeans'] = d_SmoothBinMeans.astype(np.float32)
    dicSurface['Means'] = d_BinMeans.astype(np.float32)
    dicSurface['Median'] = d_BinMedian.astype(np.float32)
    dicSurface['StDev'] = d_BinStDev.astype(np.float32)
    dicSurface['MeansInclOutliers'] = d_ProfitInclOutliers.astype(np.float32)
    dicSurface['PosiPart'] = d_PosiPart.astype(np.float32)
    dicSurface['Count'] = i_BinCount.astype(int)

    if bPlot:
        Plot2d(dicSettings, dicSurface)
    if not sFileName == '':
        SaveFASjson(sFileName, dicSettings, dicSurface)
    return dicSurface


def ProfitInBin3d(dicData, dicSettings, bPlot=True, sFileName=''):
    """
        Generates a 3d FAS surface.  Mean PnL as function of data parameters in a defined number of bins with bin size from a distribution
        Args:
            dicData: Data for the analysis. Must have dicData['d_AC'], dicData['d_Para']
                ['d_AC']: the returns of the analysed asset if dicSettings['iProfitLength'] == 0 or otherwise the price of the asset
                ['d_Para']: the parameters that the asset may depend on
            dicSettings: Settings.
                ['iProfitLength']: if iProfitLength is 0 then d_PriceOrReturn is the return else d_PriceOrReturn is the price and the return is calculated iProfitLength steps into the future
                ['bCutOutliers']: cut outliers in the return serie. Cut to border value
                ['dOutlierBorderStd']: if bCutOutliers cut outliers to this value
                ['s_DistriTypes']: choose between 'EquallySpaced', 'Normal' or 'EqualCount'
                ['i_NumberOfBins']: the number of bins in each direction. Must have tree elements
                ['iMinCount']: if the count in a bin is below iMinCount then set the bin value in SmoothMeans to NaN
                ['iSmoothStrength']: if >0 apply Gauss filter with sigma = iSmoothStrength to SmoothMeans
                ['dic_BinNames']: (optional) Names for the bins e.g. [[], ['Q1','Q2','Q3','Q4'], []]
            bPlot: Plot the surface and the event counts
                if bPlot dicSetting must include:
                    dicSettings['sSerieName']
                    dicSettings['s_ParaNames']
            sFileName: If set then save the settings and the surface to a json file
        Returns:
            The function returns a 3d FAS surface in a dictionary.
            dicSurface = {}
            dicSurface['Dimensionality'] = 3
            dicSurface['BinBorders']: the borders of the single bins. i_NumberOfBins + 1 elements
            dicSurface['SmoothMeans']: the smoothed mean value in each bin. nan if count < iMinCount. i_NumberOfBins elements
            dicSurface['Count']: the number of events in each bin. i_NumberOfBins elements
            dicSurface['Means']: the mean value in each bin. i_NumberOfBins elements
            dicSurface['Median']: the meadian value in each bin. i_NumberOfBins elements
            dicSurface['MeansInclOutliers']: the mean value including outliers in each bin. i_NumberOfBins elements
            dicSurface['StDev']: the mean value in each bin. i_NumberOfBins elements
            dicSurface['PosiPart']: the mean value in each bin. i_NumberOfBins elements
        """

    from scipy.ndimage.filters import gaussian_filter

    b_NotNans = np.logical_not(
        np.isnan(dicData['d_AC']) + np.isnan(dicData['d_Para'][0]) + np.isnan(dicData['d_Para'][1]) + np.isnan(
            dicData['d_Para'][2]))
    d_AC = dicData['d_AC'][b_NotNans]
    d_P1 = dicData['d_Para'][0][b_NotNans]
    d_P2 = dicData['d_Para'][1][b_NotNans]
    d_P3 = dicData['d_Para'][2][b_NotNans]

    if dicSettings['iProfitLength'] == 0:
        d_ProfitInclOutliers = d_AC
    else:
        d_ProfitInclOutliers = div.LogReturn(d_AC, dicSettings['iProfitLength'])

    if dicSettings['bCutOutliers']:
        d_Profit = np.clip(d_ProfitInclOutliers, -dicSettings['dOutlierBorderStd'] * np.std(d_ProfitInclOutliers),
                           dicSettings['dOutlierBorderStd'] * np.std(d_ProfitInclOutliers))
    else:
        d_Profit = d_ProfitInclOutliers

    d_P1 = np.random.normal(d_P1, 0.001 * np.std(d_P1))  # helps if the distribution has often recurring values
    d_P2 = np.random.normal(d_P2, 0.001 * np.std(d_P2))
    d_P3 = np.random.normal(d_P3, 0.001 * np.std(d_P3))
    d_BinBorders = np.array(
        [BinBorders(d_P1, dicSettings['s_DistriTypes'][0], dicSettings['i_NumberOfBins'][0]),
         BinBorders(d_P2, dicSettings['s_DistriTypes'][1], dicSettings['i_NumberOfBins'][1]),
         BinBorders(d_P3, dicSettings['s_DistriTypes'][2], dicSettings['i_NumberOfBins'][2])], dtype=object)

    d_BinTotalPnL = np.full(dicSettings['i_NumberOfBins'], np.nan)
    d_SmoothBinMeans = np.full(dicSettings['i_NumberOfBins'], np.nan)
    d_BinMeans = np.full(dicSettings['i_NumberOfBins'], np.nan)
    d_BinMedian = np.full(dicSettings['i_NumberOfBins'], np.nan)
    d_BinStDev = np.full(dicSettings['i_NumberOfBins'], np.nan)
    d_BinMeansInclOutliers = np.full(dicSettings['i_NumberOfBins'], np.nan)
    d_PosiPart = np.full(dicSettings['i_NumberOfBins'], np.nan)
    i_BinCount = np.zeros(dicSettings['i_NumberOfBins'])

    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        for iX in range(dicSettings['i_NumberOfBins'][0]):
            for iY in range(dicSettings['i_NumberOfBins'][1]):
                for iZ in range(dicSettings['i_NumberOfBins'][2]):
                    b_InRange = (d_BinBorders[0][iX] <= d_P1) & (d_P1 < d_BinBorders[0][iX + 1]) & (
                            d_BinBorders[1][iY] <= d_P2) & (d_P2 < d_BinBorders[1][iY + 1]) & (
                                        d_BinBorders[2][iZ] <= d_P3) & (d_P3 < d_BinBorders[2][iZ + 1])
                    i_BinCount[iX, iY, iZ] = sum(b_InRange)
                    d_ProfitInRange = d_Profit[b_InRange]
                    d_BinTotalPnL[iX, iY, iZ] = np.sum(d_ProfitInclOutliers[b_InRange])
                    d_BinMeans[iX, iY, iZ] = np.mean(d_ProfitInRange)
                    d_BinMedian[iX, iY, iZ] = np.nanmedian(d_ProfitInRange)
                    d_BinStDev[iX, iY, iZ] = np.std(d_ProfitInRange)
                    d_BinMeansInclOutliers[iX, iY, iZ] = np.mean(d_ProfitInclOutliers[b_InRange])
                    if sum(d_ProfitInRange != 0) > 0:
                        d_PosiPart[iX, iY, iZ] = sum(d_ProfitInRange > 0) / sum(d_ProfitInRange != 0)
                    if i_BinCount[iX, iY, iZ] > dicSettings['iMinCount']:
                        d_SmoothBinMeans[iX, iY, iZ] = np.mean(d_ProfitInRange)

        if dicSettings['iSmoothStrength'] > 0:
            d_Temp = np.zeros((dicSettings['i_NumberOfBins'][0] + 2, dicSettings['i_NumberOfBins'][1] + 2,
                               dicSettings['i_NumberOfBins'][2] + 2))  # Avoid the borders to dominate
            d_Temp[1:-1, 1:-1, 1:-1] = d_SmoothBinMeans
            d_Temp = gaussian_filter(d_Temp, sigma=dicSettings['iSmoothStrength'])
            d_SmoothBinMeans = d_Temp[1:-1, 1:-1, 1:-1]

    dicSurface = {}
    dicSurface['Dimensionality'] = 3
    dicSurface['BinBorders'] = d_BinBorders
    dicSurface['PnL'] = d_BinTotalPnL.astype(np.float32)
    dicSurface['SmoothMeans'] = d_SmoothBinMeans.astype(np.float32)
    dicSurface['Means'] = d_BinMeans.astype(np.float32)
    dicSurface['Median'] = d_BinMedian.astype(np.float32)
    dicSurface['StDev'] = d_BinStDev.astype(np.float32)
    dicSurface['MeansInclOutliers'] = d_BinMeansInclOutliers.astype(np.float32)
    dicSurface['PosiPart'] = d_PosiPart.astype(np.float32)
    dicSurface['Count'] = i_BinCount.astype(int)

    if bPlot:
        Plot3d(dicSettings, dicSurface)
    if not sFileName == '':
        SaveFASjson(sFileName, dicSettings, dicSurface)
    return dicSurface


def GenerateSurface(dicData, dicSettings=None, bPlot=True, sFileName=''):
    """
    Generates a FAS surface.  Mean PnL as function of data parameters in a defined number of bins with bin size from a distribution
    Args:
        dicData: Data for the analysis. Must have dicData['d_AC'], dicData['d_Para']
            ['d_AC']: the returns of the analysed asset if dicSettings['iProfitLength'] == 0 or otherwise the price of the asset
            ['d_Para']: the parameters that the asset may depend on
        dicSettings: Settings.
            ['iDimensionality']: desired dimensionality of surface - if None use dimensionality of dicData['d_Para']
            ['iProfitLength']: if iProfitLength is 0 then d_PriceOrReturn is the return else d_PriceOrReturn is the price and the return is calculated iProfitLength steps into the future
            ['bCutOutliers']: cut outliers in the return serie. Cut to border value
            ['dOutlierBorderStd']: if bCutOutliers cut outliers to this value
            ['s_DistriTypes']: choose between 'EquallySpaced', 'Normal' or 'EqualCount'
            ['i_NumberOfBins']: the number of bins in each direction. Must have at least iDimensionality elements
            ['iMinCount']: if the count in a bin is below iMinCount then set the bin value in SmoothMeans to NaN
            ['iSmoothStrength']: if >0 apply Gauss filter with sigma = iSmoothStrength to SmoothMeans
            ['dic_BinNames']: (optional) Names for the bins e.g. [[], ['Q1','Q2','Q3','Q4'], []]
        bPlot: Plot the surface and the event counts
            if bPlot dicSetting must include:
                dicSettings['sSerieName']
                dicSettings['s_ParaNames']
        sFileName: If set then save the settings and the surface to a json file
    Returns:
        The function returns a FAS surface in a dictionary.
        dicSurface = {}
        dicSurface['Dimensionality']: from dicSettings or dimensionality of data
        dicSurface['BinBorders']: the borders of the single bins. i_NumberOfBins + 1 elements
        dicSurface['SmoothMeans']: the smoothed mean value in each bin. nan if count < iMinCount. i_NumberOfBins elements
        dicSurface['Count']: the number of events in each bin. i_NumberOfBins elements
        dicSurface['Means']: the mean value in each bin. i_NumberOfBins elements
        dicSurface['Median']: the meadian value in each bin. i_NumberOfBins elements
        dicSurface['MeansInclOutliers']: the mean value including outliers in each bin. i_NumberOfBins elements
        dicSurface['StDev']: the mean value in each bin. i_NumberOfBins elements
        dicSurface['PosiPart']: the mean value in each bin. i_NumberOfBins elements
    """
    for d_P in dicData['d_Para']:
        if len(d_P)!=len(dicData['d_AC']):
            div.Error('The parameters vectors must have the same length as the returns vector')
    if dicSettings is None:
        dicSettings = DefaultSettingsDict()
    if dicSettings['iDimensionality'] is None:
        dicSettings['iDimensionality'] = len(dicData['d_Para'])
    if dicSettings['iDimensionality'] == 1:
        return ProfitInBin1d(dicData, dicSettings, bPlot, sFileName)
    elif dicSettings['iDimensionality'] == 2:
        return ProfitInBin2d(dicData, dicSettings, bPlot, sFileName)
    elif dicSettings['iDimensionality'] == 3:
        return ProfitInBin3d(dicData, dicSettings, bPlot, sFileName)
    else:
        div.Error('Wrong dimensionality')
